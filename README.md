# Citas Medicas

Aplicación desarrollada como prueba técnica para Tempe.

La aplicación citas medicas ofrece una api-rest para la gestión de personal medico/paciente y la gestión de citas medicas. Se da la posibilidad de ver, crear, editar y eliminar personal, gestionando de la misma manera las citas y los diagnosticos de dichas citas, también se puede ver el listado de citas para cada uno de los medicos o clientes del sistema.

## Diagramas

El diagrama propocionado ha sido el siguiente:

![img1](./docs/images/class.jpg)


## Get started

### Clonar el repositorio

```shell
git clone https://gitlab.com/arl3/citasmedicas.git
```

**Nota:** En el repositorio se han subido distintos tipos de ficheros que no deben de subirse, pero para un despliegue automatizado era necesario (.jar, /dist, etc.), esto no habría que subirlo si estamos en un entorno de desarrollo real.


#### Seguir los siguientes pasos si se quiere desplegar en desarrollo:

Para levantar la base de datos, ejecutamos el siguiente comando para crear una BBDD postgres, esto levantará un servicio en _Docker_:
```shell
docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d -p 5432:5432 postgres
```
Mediante un IDE hemos arrancado la aplicación api-medical.

### Ejecutar el siguiente comando para un despliegue automatizado con docker:

Para generar el .jar hemos ejecutado el comando de Maven build, ya sea utilizando un IDE o desde una terminal.
Una vez generado el .jar, tenemos que situarnos en la carpeta raíz del proyecto y ejecutar el siguiente comando:

```shell
docker-compose -f docker-compose.yml -p apiMedical up -d --build
```

Para parar la aplicación podemos ejecutar el siguiente comando:

```shell
docker-compose -p apiMedical down
```

#### Los servicios se levantan en los siguientes puertos:

BACKEND_NAME: cites-backend:8080
DATABASE: postgresql:5432

### Para probar la aplicación, debemos usar un cliente de peticiones http, por ejemplo Postman:

En [este](http://localhost:8080/swagger-ui.html) link podemos encontrar una documentación con swagger de las acciones que han sido desarrolladas para esta demostración



### Vías futuras:

A continuación, se listaran una serie de funcionalidad que no ha podido ser desarrollada por falta de tiempo:

* Gestión de login en la aplicación usando un token con jwt.
* Implementar el patrón criteria para realizar busquedas avanzadas de los distintas entidades de la aplicaión.
* Utilizar modelMapper para el mapeo de las entidades.
* Completar el resto de test de la aplicación.
